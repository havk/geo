import sys
import Image
import ImageOps
import math
from PyQt4 import QtGui, QtCore

class MyApp(QtGui.QMainWindow):

    def __init__(self):
        super(MyApp, self).__init__()

        self.classes = []
        self.selecting = False
        self.r = 5
        self.printer = QtGui.QPrinter()
        self.scaleFactor = 0.0

        self.vbox = QtGui.QHBoxLayout()
        self.widget = QtGui.QWidget()
        self.widget.setLayout(self.vbox)

        self.imageLabel = QtGui.QLabel()
        self.imageLabel.setBackgroundRole(QtGui.QPalette.Base)
        self.imageLabel.setSizePolicy(QtGui.QSizePolicy.Ignored,
                QtGui.QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)

        self.imageLabel.mousePressEvent = self.mouseClick
        self.imageLabel.mouseMoveEvent = self.mouseMove
        self.imageLabel.mouseReleaseEvent = self.mouseRelease
        self.imageLabel.rubberBand = QtGui.QRubberBand(QtGui.QRubberBand.Rectangle, self.imageLabel)
        self.imageLabel.origin = QtCore.QPoint()

        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)


        self.imageLabel2 = QtGui.QLabel()
        self.imageLabel2.setBackgroundRole(QtGui.QPalette.Base)
        self.imageLabel2.setSizePolicy(QtGui.QSizePolicy.Ignored,
                QtGui.QSizePolicy.Ignored)
        self.imageLabel2.setScaledContents(True)

        self.scrollArea2 = QtGui.QScrollArea()
        self.scrollArea2.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea2.setWidget(self.imageLabel2)

        self.vbox.addWidget(self.scrollArea)
        self.vbox.addWidget(self.scrollArea2)
        self.setCentralWidget(self.widget)

        self.createActions()
        self.createMenus()
        self.createToolbar()

        self.setWindowTitle("Image Classifier")
        self.resize(1200, 800)

    def createImageLabels(self):
        self.imageLabel = QtGui.QLabel()
        self.imageLabel.setBackgroundRole(QtGui.QPalette.Base)
        self.imageLabel.setSizePolicy(QtGui.QSizePolicy.Ignored,
                QtGui.QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)

        self.scrollArea = QtGui.QScrollArea()
        self.scrollArea.setBackgroundRole(QtGui.QPalette.Dark)
        self.scrollArea.setWidget(self.imageLabel)
        self.setCentralWidget(self.scrollArea)

    def open(self):
        self.fileName = QtGui.QFileDialog.getOpenFileName(self, "Open File",
                QtCore.QDir.currentPath())
        if self.fileName:
            image = QtGui.QImage(self.fileName)
            if image.isNull():
                QtGui.QMessageBox.information(self, "Image Viewer",
                        "Cannot load %s." % self.fileName)
                return

            self.imageLabel.setPixmap(QtGui.QPixmap.fromImage(image))
            self.scaleFactor = 1.0

            self.printAct.setEnabled(True)
            self.fitToWindowAct.setEnabled(True)
            self.meanAct.setEnabled(True)
            self.standardDeviatonAct.setEnabled(True)
            self.kurtosisAct.setEnabled(True)
            self.asymmetryAct.setEnabled(True)
            #self.runAct.setEnabled(True)
            self.selectAct.setEnabled(True)
            self.updateActions()

            if not self.fitToWindowAct.isChecked():
                self.imageLabel.adjustSize()

    def print_(self):
        dialog = QtGui.QPrintDialog(self.printer, self)
        if dialog.exec_():
            painter = QtGui.QPainter(self.printer)
            rect = painter.viewport()
            size = self.imageLabel.pixmap().size()
            size.scale(rect.size(), QtCore.Qt.KeepAspectRatio)
            painter.setViewport(rect.x(), rect.y(), size.width(), size.height())
            painter.setWindow(self.imageLabel.pixmap().rect())
            painter.drawPixmap(0, 0, self.imageLabel.pixmap())

    def zoomIn(self):
        self.scaleImage(1.25)

    def zoomOut(self):
        self.scaleImage(0.8)

    def normalSize(self):
        self.imageLabel.adjustSize()
        self.imageLabel2.adjustSize()
        self.scaleFactor = 1.0

    def fitToWindow(self):
        fitToWindow = self.fitToWindowAct.isChecked()
        self.scrollArea.setWidgetResizable(fitToWindow)
        if not fitToWindow:
            self.normalSize()

        self.updateActions()

    def about(self):
        QtGui.QMessageBox.about(self, "About Image Viewer",
                "<p>The <b>Image Viewer</b> example shows how to combine "
                "QLabel and QScrollArea to display an image. QLabel is "
                "typically used for displaying text, but it can also display "
                "an image. QScrollArea provides a scrolling view around "
                "another widget. If the child widget exceeds the size of the "
                "frame, QScrollArea automatically provides scroll bars.</p>"
                "<p>The example demonstrates how QLabel's ability to scale "
                "its contents (QLabel.scaledContents), and QScrollArea's "
                "ability to automatically resize its contents "
                "(QScrollArea.widgetResizable), can be used to implement "
                "zooming and scaling features.</p>"
                "<p>In addition the example shows how to use QPainter to "
                "print an image.</p>")

    def createActions(self):
        self.openAct = QtGui.QAction("&Open...", self, shortcut="Ctrl+O",
                triggered=self.open)

        self.printAct = QtGui.QAction("&Print...", self, shortcut="Ctrl+P",
                enabled=False, triggered=self.print_)

        self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",
                triggered=self.close)

        self.zoomInAct = QtGui.QAction("Zoom &In (25%)", self,
                shortcut="Ctrl++", enabled=False, triggered=self.zoomIn)

        self.zoomOutAct = QtGui.QAction("Zoom &Out (25%)", self,
                shortcut="Ctrl+-", enabled=False, triggered=self.zoomOut)

        self.normalSizeAct = QtGui.QAction("&Normal Size", self,
                shortcut="Ctrl+S", enabled=False, triggered=self.normalSize)

        self.fitToWindowAct = QtGui.QAction("&Fit to Window", self,
                enabled=False, checkable=True, shortcut="Ctrl+F",
                triggered=self.fitToWindow)

        self.aboutAct = QtGui.QAction("&About", self, triggered=self.about)

        self.aboutQtAct = QtGui.QAction("About &Qt", self,
                triggered=QtGui.qApp.aboutQt)

        self.meanAct = QtGui.QAction("&Mean", self,
                enabled=False, checkable=True)

        self.standardDeviatonAct = QtGui.QAction("&Standard Deviation", self,
                enabled=False, checkable=True)

        self.kurtosisAct = QtGui.QAction("&Kurtosis", self,
                enabled=False, checkable=True)

        self.asymmetryAct = QtGui.QAction("&Asymmtetry", self,
                enabled=False, checkable=True)

        self.setRAct = QtGui.QAction("Select radius", self,
                enabled=True, triggered=self.setR)

        self.selectAct = QtGui.QAction("&Select classes", self,
                enabled=False, triggered=self.selectClasses)

        self.runAct = QtGui.QAction("&Run", self,
                enabled=False, triggered=self.run)

    def createMenus(self):
        self.fileMenu = QtGui.QMenu("&File", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addAction(self.printAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.viewMenu = QtGui.QMenu("&View", self)
        self.viewMenu.addAction(self.zoomInAct)
        self.viewMenu.addAction(self.zoomOutAct)
        self.viewMenu.addAction(self.normalSizeAct)
        self.viewMenu.addSeparator()
        self.viewMenu.addAction(self.fitToWindowAct)

        self.helpMenu = QtGui.QMenu("&Help", self)
        self.helpMenu.addAction(self.aboutAct)
        self.helpMenu.addAction(self.aboutQtAct)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.viewMenu)
        self.menuBar().addMenu(self.helpMenu)

    def updateActions(self):
        self.zoomInAct.setEnabled(not self.fitToWindowAct.isChecked())
        self.zoomOutAct.setEnabled(not self.fitToWindowAct.isChecked())
        self.normalSizeAct.setEnabled(not self.fitToWindowAct.isChecked())

    def createToolbar(self):
        self.toolbar = self.addToolBar('Classification');
        self.toolbar.addAction(self.meanAct)
        self.toolbar.addAction(self.standardDeviatonAct)
        self.toolbar.addAction(self.kurtosisAct)
        self.toolbar.addAction(self.asymmetryAct)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.setRAct)
        self.toolbar.addAction(self.selectAct)
        self.toolbar.addAction(self.runAct)

    def scaleImage(self, factor):
        self.scaleFactor *= factor
        self.imageLabel.resize(self.scaleFactor * self.imageLabel.pixmap().size())

        self.adjustScrollBar(self.scrollArea.horizontalScrollBar(), factor)
        self.adjustScrollBar(self.scrollArea.verticalScrollBar(), factor)

        self.zoomInAct.setEnabled(self.scaleFactor < 3.0)
        self.zoomOutAct.setEnabled(self.scaleFactor > 0.333)

    def adjustScrollBar(self, scrollBar, factor):
        scrollBar.setValue(int(factor * scrollBar.value()
                                + ((factor - 1) * scrollBar.pageStep()/2)))

    def selectClasses(self):
        self.selecting = True
        self.classes = []
        self.runAct.setEnabled(True)

    def run(self):
        self.selecting = False
        pix, width, height = self.getPixels(self.fileName)
        classes = self.getClasses()
        pixStats, clsStats = self.countStatistics(pix, width, height)

        if len(pixStats) > 0:
            classified = self.classify(pixStats, clsStats, classes)
            self.drawResult(classified)

    def setR(self):
        text, ok = QtGui.QInputDialog.getText(self, 'Input Dialog', 'Select radius: ')
        if ok:
            self.r = (int(text))

    def getPixels(self, fileName):
        im = Image.open(str(fileName))
        im = ImageOps.grayscale(im)
        pix = im.load()
        width, height = im.size

        return pix, width, height

    def countStatistics(self, pix, width, height):
        stats = self.getImageStats(pix, width, height)
        clsStats = self.getClsStats(pix)

        return stats, clsStats

    def getImageStats(self, pix, width, height):
        stats = []

        meanList =[[] for i in range(width)]
        standardDeviationList =[[] for i in range(width)]
        kurtosisList =[[] for i in range(width)]
        asymmetryList =[[] for i in range(width)]

        for w in range(width):
            for h in range(height):
                s = 0;
                n = 0;
                for i in range(w - self.r, w + self.r):
                    for j in range (h - self.r, h + self.r):
                        if i >= 0 and i < width and j >=0 and j < height:
                            s += pix[i,j]
                            n += 1

                mean = s/n
                meanList[w].append(mean)

                s1, s2, s3 = 0, 0 ,0;
                for i in range(w - self.r, w + self.r):
                    for j in range (h - self.r, h + self.r):
                        if i >= 0 and i < width and j >=0 and j < height:
                            s1 += math.pow((pix[i,j] - mean), 2)
                            s2 += math.pow((pix[i,j] - mean), 4)
                            s3 += math.pow((pix[i,j] - mean), 3)

                standardDeviation = math.sqrt(s1 / n)
                standardDeviationList[w].append(standardDeviation)


                if standardDeviation != 0:
                    kurtosis = s2 / (n * math.pow(standardDeviation, 4))
                    asymmetry = s3 / (n * math.pow(standardDeviation, 3));
                else:
                    kurtosis = 0
                    asymmetry = 0

                kurtosisList[w].append(kurtosis)
                asymmetryList[w].append(asymmetry)


        if self.meanAct.isChecked():
            stats.append(meanList)

        if self.standardDeviatonAct.isChecked():
            stats.append(standardDeviationList)

        if self.kurtosisAct.isChecked():
            stats.append(kurtosisList)

        if self.asymmetryAct.isChecked():
            stats.append(asymmetryList)

        return stats

    def getClsStats(self, pix):
        clsStats = []

        for i in range(0, len(self.classes)):
            cls = []
            leftTop = self.classes[i][0]
            rightBottom = self.classes[i][1]

            mean = self.countMeansCls(pix, leftTop, rightBottom)
            standardDeviation = self.countStandardDeviationCls(pix, leftTop, rightBottom, mean)

            kurtosis = self.countStandardKurtosisCls(pix, leftTop, rightBottom,
                                                        mean, standardDeviation)

            asymmetry = self.countAsymmetryCls(pix, leftTop, rightBottom,
                                                mean, standardDeviation)

            if self.meanAct.isChecked():
                cls.append(mean)

            if self.standardDeviatonAct.isChecked():
                cls.append(standardDeviation)

            if self.kurtosisAct.isChecked():
                cls.append(kurtosis)

            if self.asymmetryAct.isChecked():
                cls.append(asymmetry)

            clsStats.append(cls)

        return clsStats

    def countMeansCls(self, pix, leftTop, rightBottom):
        s = 0;
        n = 0;

        for w in range(leftTop[0], rightBottom[0]):
            for h in range(leftTop[1], rightBottom[1]):
                s += pix[w, h]
                #print(pix[w, h])
                n += 1

        val = s/n

        return val

    def countStandardDeviationCls(self, pix, leftTop, rightBottom, mean):
        s = 0;
        n = 0;

        for w in range(leftTop[0], rightBottom[0]):
            for h in range(leftTop[1], rightBottom[1]):
                s += math.pow((pix[w, h] - mean), 2)
                n += 1

        val = math.sqrt(s / n)

        return val

    def countStandardKurtosisCls(self, pix, leftTop, rightBottom, mean, standardDeviation):
        s = 0;
        n = 0;

        for w in range(leftTop[0], rightBottom[0]):
            for h in range(leftTop[1], rightBottom[1]):
                s += math.pow((pix[w, h] - mean), 3)
                n += 1

        if standardDeviation != 0:
            kur = s / (n * math.pow(standardDeviation, 4))
        else:
            kur = 0

        return kur

    def countAsymmetryCls(self, pix, leftTop, rightBottom, mean, standardDeviation):
        s = 0;
        n = 0;

        for w in range(leftTop[0], rightBottom[0]):
            for h in range(leftTop[1], rightBottom[1]):
                s += math.pow((pix[w, h] - mean), 3)
                n += 1

        if standardDeviation != 0:
            asm = s / (n * math.pow(standardDeviation, 3))
        else:
            asm = 0

        return asm

    def getClasses(self):
        l = len(self.classes)
        step = 0

        if l > 1:
            step = (254 / (l-1))

        #print(l, step)

        c = [i * step  for i in range(0, l)]

        return c

    def classify(self, pixStats, clsStats, classes):
        width, height = (len(pixStats[0]), len(pixStats[0][0]))

        classified =[[0 for x in range(height)] for i in range(width)]
        for w in range(width):
            for h in range(height):
                clsNr = self.getClsNr(w, h, pixStats, clsStats)
                classified[w][h] = classes[clsNr]

        return classified

    def getClsNr(self, w, h, pixStats, clsStats):
        clsSum = [0 for i in range(len(self.classes))]

        for c in range(len(self.classes)):
            for s in range(len(pixStats)):
                clsSum[c] += abs(pixStats[s][w][h] - clsStats[c][s])

        minimum = min(clsSum)
        cls = 0;
        for i in range(len(clsSum)):
            if clsSum[i] == minimum:
                cls = i

        return cls

    def drawResult(self, classified):
        size = (len(classified), len(classified[0]))
        newIm = Image.new('L', size)
        pixNew = newIm.load()

        width, height = size
        for w in range(width):
            for h in range(height):
                pixNew[w, h] = classified[w][h]

        newIm.save("tmp.jpg")
        qIm = QtGui.QImage("tmp.jpg")
        self.imageLabel2.setPixmap(QtGui.QPixmap.fromImage(qIm))
        self.normalSize()

    def mouseClick(self, event):
        point = event.pos()
        self.point1 = point

        if event.button() == QtCore.Qt.LeftButton:
            self.imageLabel.origin = QtCore.QPoint(point)
            self.imageLabel.rubberBand.setGeometry(QtCore.QRect(self.imageLabel.origin, QtCore.QSize()))
            self.imageLabel.rubberBand.show()

    def mouseMove(self, event):
        point = event.pos()
        self.point2 = point
        if not self.imageLabel.origin.isNull():
            self.imageLabel.rubberBand.setGeometry(QtCore.QRect(self.imageLabel.origin, point).normalized())

    def mouseRelease(self, event):
        if self.selecting:
            self.addClass()

        if event.button() == QtCore.Qt.LeftButton:
            self.imageLabel.rubberBand.hide()

    def addClass(self):
        leftTopX = min(self.point1.x(), self.point2.x())
        leftTopY = min(self.point1.y(), self.point2.y())
        rightBottomX = max(self.point1.x(), self.point2.x())
        rightBottomY = max(self.point1.y(), self.point2.y())

        leftTop = (leftTopX, leftTopY)
        rightBottom = (rightBottomX, rightBottomY)

        self.classes.append((leftTop, rightBottom))


def main():

    app = QtGui.QApplication(sys.argv)
    myApp = MyApp()
    myApp.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
